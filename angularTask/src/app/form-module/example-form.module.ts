import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormModuleContainerComponent } from './form-module-container/form-module-container.component';
import { RouterModule, Routes } from '@angular/router';
import { PersonalFormInfoComponent } from './personal-form-info/personal-form-info.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ListInfoComponent } from './list-info/list-info.component';


const routes: Routes = [
  {
    path: '',
    component: FormModuleContainerComponent,
    children:[
      {
        path: '',
        component: PersonalFormInfoComponent,
      },
      {
        path: 'personal-info-form',
        component: PersonalFormInfoComponent,
      },{
        path: 'personal-info-form/:id',
        component: PersonalFormInfoComponent,
      },
      {
        path: 'list-info',
        component: ListInfoComponent,
      },
  ]}]

@NgModule({
  declarations: [ PersonalFormInfoComponent,FormModuleContainerComponent,ListInfoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule
   
  ],
  exports:[RouterModule]
})
export class ExampleFormModule { }
