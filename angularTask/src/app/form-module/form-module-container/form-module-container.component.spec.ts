import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormModuleContainerComponent } from './form-module-container.component';

describe('FormModuleContainerComponent', () => {
  let component: FormModuleContainerComponent;
  let fixture: ComponentFixture<FormModuleContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormModuleContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormModuleContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
