import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalFormInfoComponent } from './personal-form-info.component';

describe('PersonalFormInfoComponent', () => {
  let component: PersonalFormInfoComponent;
  let fixture: ComponentFixture<PersonalFormInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalFormInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalFormInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
