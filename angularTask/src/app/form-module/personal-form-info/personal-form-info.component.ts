import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators, FormGroupDirective } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/app-module/services/global.service';


@Component({
  selector: 'app-personal-form-info',
  templateUrl: './personal-form-info.component.html',
  styleUrls: ['./personal-form-info.component.scss']
})
export class PersonalFormInfoComponent implements OnInit {

  
  personal_info_form = new FormGroup({
    fullName: new FormControl('',Validators.required),
    fathersName: new FormControl('',Validators.required),
    email:new FormControl('',[Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    phone_number:new FormControl('',[Validators.required,Validators.pattern(/^[6-9]\d{9}$/)]),
    
    qualification: new FormGroup({
      secondary_education: new FormGroup({
        schoolname: new FormControl('',Validators.required),
        percentage_obtained: new FormControl('',[Validators.required, Validators.max(100)])
      }),
      higher_secondary: new FormGroup({
        schoolname: new FormControl('',Validators.required),
        percentage_obtained: new FormControl('',[Validators.required, Validators.max(100)])
      }),
      bachelors: new FormGroup({
        collegename: new FormControl('',Validators.required),
        percentage_obtained: new FormControl('',[Validators.required, Validators.max(100)])
      }),

    }),

    address: new FormGroup({
      state: new FormControl('',Validators.required),
      district: new FormControl('',Validators.required),
      city: new FormControl('',Validators.required),
      landmark: new FormControl('',Validators.required),
      pincode: new FormControl('',[Validators.required,Validators.pattern('^[1-9][0-9]{5}$')])
    }),

    hobbies: new FormArray([
      new FormControl('',Validators.required),
      new FormControl('',Validators.required),
    ])

  });
  editFlag = false;
  paramid
  
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(private global:GlobalService, private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    console.log(this.global.personal_info)
    this.paramid = this.route.snapshot.paramMap.get('id');
    if(this.paramid){
      this.editFlag = true;
      this.personal_info_form.patchValue({
        fullName:this.global.personal_info[this.paramid].fullName,
        fathersName:this.global.personal_info[this.paramid].fathersName,
        email: this.global.personal_info[this.paramid].email,
        phone_number: this.global.personal_info[this.paramid].phone_number,
        qualification:this.global.personal_info[this.paramid].qualification,
        address:this.global.personal_info[this.paramid].address,
        hobbies :this.global.personal_info[this.paramid].hobbies
      })
      //this.hobbies.patchValue(this.global.personal_info[this.paramid].hobbies)
    }
   
   
  }

  trackByFn(index: any, item: any) {
    return index;
  }

  get hobbies():FormArray{
    return this.personal_info_form.get('hobbies') as FormArray;
  }

  addHobby(){
    this.hobbies.push(new FormControl(''),);
  }

  resethobbies(){
    this.hobbies.patchValue(['',])
  }

  onSubmit(){
    if(this.editFlag == true){
      this.global.personal_info[this.paramid] =this.personal_info_form.value
      console.log(this.global.personal_info[this.paramid])
      this.formGroupDirective.resetForm();
      this.router.navigate(['/personal-info-form']);
    }else{
      this.global.personal_info.push(this.personal_info_form.value)
      this.formGroupDirective.resetForm();
    }
    
  }

}
