import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/app-module/services/global.service';



@Component({
  selector: 'app-list-info',
  templateUrl: './list-info.component.html',
  styleUrls: ['./list-info.component.scss']
})
export class ListInfoComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'full_name', 'fathersName', 'email', 'phone_number', 'qualification_bachelors_collegename', 'qualification_bachelors_percentage_obtained', 'higher_secondary1', 'higher_secondary2'
  , 'secondary_education1', 'secondary_education2', 'state', 'district', 'city', 'landmark', 'pincode','operations'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private global:GlobalService, private router: Router ) {
    
    console.log(this.global.personal_info)
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.global.personal_info);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  delData(id){
    console.log(id)

  }
  edit(id){
    console.log(id)
    this.router.navigate(['/personal-info-form', { id: id }]);
  }
}


