import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  personal_info = []
  constructor() { 
  this.personal_info = [  
    {
      address: {state: "Delhi", district: "Delhi", city: "Laxminagar", landmark: "Saintmathews Church", pincode: "121003"},
      email: "prince.shah97@gmail.com",
      fathersName: "Binod Shah",
      fullName: "prince.shah97@gmail.com",
      phone_number: "9896123158",
      qualification: {
        bachelors: {collegename: "Rawal College", percentage_obtained: "70"},
        higher_secondary: {schoolname: "Manimukunda", percentage_obtained: "75"},
        secondary_education: {schoolname: "New Life School", percentage_obtained: "85"}
     },
     hobbies:['football',]
  }
  ]

    }
}
