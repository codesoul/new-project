import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
 
  // {
  //   path: '',
  //   component: PersonalInformationComponent,
  // },
  // {
  //   path: 'personal-info-form',
  //   component: PersonalInformationComponent,
  // },
  // {
  //   path: 'registered-user-list',
  //   component: ListPersonalInformationComponent,
  // },
 
  {
    path: '',
    loadChildren: () => import('../app/form-module/example-form.module').then(m => m.ExampleFormModule),
   
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
